class Token {
  /**
   * @param {string} type
   * @param {[number, number]} start
   * @param {[number, number]} end
   * @param {any} value
   */
  constructor(type, start, end, value) {
    this.type = type;
    this.start = start;
    this.end = end;
    this.value = value;
  }
}

class CharacterStream {
  /**
   * @param {string} input
   */
  constructor(input) {
    this.input = input;
    this.pos = 0;
    this.line = 1;
    this.col = 0;
  }

  next(n = 1) {
    let str = "";

    while (!this.eof() && n-- > 0) {
      const ch = this.input.charAt(this.pos++);

      if (ch === "\n") {
        this.line++;
        this.col = 0;
      } else {
        this.col++;
      }

      str += ch;
    }

    return str;
  }

  peek(n = 1) {
    return this.input.slice(this.pos, this.pos + n);
  }

  eof() {
    return this.peek() === "";
  }

  error(msg) {
    throw new Error(`${msg} (${this.line}:${this.col})`);
  }

  *[Symbol.iterator]() {
    while (!this.eof()) {
      yield this.next();
    }
  }
}

class TokenStream {
  /**
   * @param {CharacterStream} input
   */
  constructor(input) {
    this.input = input;
    this.current = null;
  }

  /**
   * @returns {Token} The next token in the stream
   */
  next() {
    if (this.current) {
      const value = this.current;
      this.current = null;

      return value;
    }

    this.while(this.isWhitespace);

    const start = [this.input.line, this.input.col];

    if (this.input.eof()) {
      return new Token("eof", start, start);
    }

    const ch = this.input.peek();

    if (ch === ";") {
      this.input.next();
      const comment = this.line();
      const end = [this.input.line, this.input.col];
      return new Token("comment", start, end, comment);
    }

    if (ch === '"') {
      const string = this.string();
      const end = [this.input.line, this.input.col];
      return new Token("string", start, end, string);
    }

    if (ch === "'") {
      const string = this.singleString();
      const end = [this.input.line, this.input.col];
      return new Token("string", start, end, string);
    }

    if (ch >= "0" && ch <= "9") {
      const number = this.number();
      const end = [this.input.line, this.input.col];
      return new Token("number", start, end, number);
    }

    if (ch === "-") {
      let digit = this.input.peek(2)[1];

      if (digit >= "0" && digit <= "9") {
        const number = this.number();
        const end = [this.input.line, this.input.col];
        return new Token("number", start, end, number);
      }
    } else if (ch === "f") {
      if (this.input.peek(5) === "false") {
        this.input.next(5);
        const end = [this.input.line, this.input.col];
        return new Token("boolean", start, end, false);
      }
    } else if (ch === "t") {
      if (this.input.peek(4) === "true") {
        this.input.next(4);
        const end = [this.input.line, this.input.col];
        return new Token("boolean", start, end, true);
      }
    } else if (ch === "n") {
      if (this.input.peek(4) === "null") {
        this.input.next(4);
        const end = [this.input.line, this.input.col];
        return new Token("null", start, end, null);
      }
    } else if (this.isPunctuation(ch)) {
      this.input.next();
      const end = [this.input.line, this.input.col];
      return new Token("punctuation", start, end, ch);
    } else if (this.isControlCharacter(ch)) {
      throw new Error(
        `Unexpected character ${ch} (${start[0]}:${start[1] + 1})`
      );
    }

    const atom = this.while(
      (ch) =>
        !this.isPunctuation(ch) &&
        !this.isWhitespace(ch) &&
        !this.isControlCharacter(ch)
    );
    const end = [this.input.line, this.input.col];

    return new Token("atom", start, end, atom);
  }

  peek() {
    if (!this.current) {
      this.current = this.next();
    }

    return this.current;
  }

  line() {
    return this.while((ch) => ch !== "\n");
  }

  number() {
    let step = "init";
    let isNegative = false;
    let valid = true;

    const rawNumber = this.while((ch) => {
      if (!valid) {
        return (
          !this.isWhitespace(ch) &&
          !this.isPunctuation(ch) &&
          !this.isControlCharacter(ch)
        );
      }

      if (step === "init") {
        // initial empty string, check if there is a negative sign or not

        if (ch === "-") {
          // negative number
          // now see if the number has an alternate base
          step = "negative";
          isNegative = true;
          return true;
        }

        if (ch === "0") {
          // positive number with alternate base
          step = "base";
          return true;
        }

        if (ch >= "1" && ch <= "9") {
          // positive base 10 number
          // get rest of the integer part
          step = "integer";
          return true;
        }
      } else if (step === "negative") {
        // "-"

        if (ch === "0") {
          // number with alternate base
          step = "base";
          return true;
        }

        if (ch >= "1" && ch <= "9") {
          // base 10 number
          // get rest of the integer part
          step = "integer";
          return true;
        }
      } else if (step === "base") {
        // "0" or "-0"

        if (ch === "b") {
          // binary number
          step = "binary";
          return true;
        }

        if (ch === "o") {
          // octal number
          step = "octal";
          return true;
        }

        if (ch === "x") {
          // hex number
          step = "hex";
          return true;
        }

        if (ch === "_" || (ch >= "0" && ch <= "9")) {
          // found base 10 number
          // get rest of the integer part
          step = "integer";
          return true;
        }

        if (
          this.isWhitespace(ch) ||
          this.isPunctuation(ch) ||
          this.isControlCharacter(ch)
        ) {
          return false;
        }
      } else if (step === "binary") {
        if (ch === "_" || ch === "0" || ch === "1") {
          return true;
        }

        if (
          this.isWhitespace(ch) ||
          this.isPunctuation(ch) ||
          this.isControlCharacter(ch)
        ) {
          return false;
        }
      } else if (step === "octal") {
        if (ch === "_" || (ch >= "0" && ch <= "7")) {
          return true;
        }

        if (
          this.isWhitespace(ch) ||
          this.isPunctuation(ch) ||
          this.isControlCharacter(ch)
        ) {
          return false;
        }
      } else if (step === "hex") {
        if (
          ch === "_" ||
          (ch >= "0" && ch <= "9") ||
          (ch >= "a" && ch <= "f") ||
          (ch >= "A" && ch <= "F")
        ) {
          return true;
        }

        if (
          this.isWhitespace(ch) ||
          this.isPunctuation(ch) ||
          this.isControlCharacter(ch)
        ) {
          return false;
        }
      } else if (step === "integer") {
        if (ch === "_" || (ch >= "0" && ch <= "9")) {
          return true;
        }

        if (ch === ".") {
          step = "fraction_init";
          return true;
        }

        if (
          this.isWhitespace(ch) ||
          this.isPunctuation(ch) ||
          this.isControlCharacter(ch)
        ) {
          return false;
        }
      } else if (step === "fraction_init") {
        // "nnn."
        // only allow numbers not _

        if (ch >= "0" && ch <= "9") {
          step = "fraction";
          return true;
        }
      } else if (step === "fraction") {
        // "nnn.n"

        if (ch === "_" || (ch >= "0" && ch <= "9")) {
          return true;
        }

        if (ch === "e" || ch === "E") {
          step === "exponent_init";
          return true;
        }

        if (
          this.isWhitespace(ch) ||
          this.isPunctuation(ch) ||
          this.isControlCharacter(ch)
        ) {
          return false;
        }
      } else if (step === "exponent_init") {
        // "nnn.nnE"

        if (ch === "-" || ch === "+") {
          step === "exponent_sign";
          return true;
        }

        if (ch >= "0" && ch <= "9") {
          step = "exponent";
          return true;
        }
      } else if (step === "exponent_sign") {
        // "nn.nnE-" or "nn.nnE+"

        if (ch >= "0" && ch <= "9") {
          step = "exponent";
          return true;
        }
      } else if (step === "exponent") {
        // "nn.nnEn"

        if (ch >= "0" && ch <= "9") {
          return true;
        }

        if (
          this.isWhitespace(ch) ||
          this.isPunctuation(ch) ||
          this.isControlCharacter(ch)
        ) {
          return false;
        }
      }

      valid = false;
      return true;
    });

    if (!valid) {
      // throws
      this.input.error(`Invalid number: ${rawNumber}`);
    }

    const number =
      Number(rawNumber.slice(isNegative ? 1 : 0)) * (isNegative ? -1 : 1);
    return { number, rawNumber };
  }

  string() {
    if (this.input.next() !== '"') {
      // throws
      this.input.error("Invalid string");
    }

    let escaped = false;
    const escapes = [];
    let i = -1;

    const rawString = this.while((ch) => {
      if (this.isControlCharacter(ch)) {
        // throws
        this.input.error(
          "Invalid string contains a control character: 0x" +
            codePoint.toString(16).toUpperCase().padStart(2, "0")
        );
      }

      i++;

      if (escaped) {
        escapes.push(i);
        escaped = false;
        return true;
      }

      if (ch === "\\") {
        escaped = true;
        return true;
      }

      return ch !== '"';
    });

    if (this.input.next() !== '"') {
      // throws
      this.input.error('Invalid string: "' + rawString);
    }

    let escapedString = rawString;
    let removed = 0;

    for (let escape of escapes) {
      escape -= removed;
      const ch = escapedString.charAt(escape);

      if (
        ch !== '"' &&
        ch !== "\\" &&
        ch !== "/" &&
        ch !== "b" &&
        ch !== "f" &&
        ch !== "n" &&
        ch !== "r" &&
        ch !== "t" &&
        ch !== "u"
      ) {
        // drop the "\" preceeding
        escapedString =
          escapedString.slice(0, escape - 1) + escapedString.slice(escape);
        removed++;
      }
    }

    try {
      /**
       * @type {string}
       */
      const string = JSON.parse(`"${escapedString}"`);
      return { string, rawString, quote: "double" };
    } catch (e) {
      // throws
      this.input.error(e.message + ` "${escapedString}"`);
    }
  }

  singleString() {
    if (this.input.next() !== "'") {
      // throws
      this.input.error("Invalid string");
    }

    let escaped = false;
    const quotes = [];
    const escapes = [];
    let i = -1;

    const rawString = this.while((ch) => {
      if (this.isControlCharacter(ch)) {
        // throws
        this.input.error(
          "Invalid string contains a control character: 0x" +
            codePoint.toString(16).toUpperCase().padStart(2, "0")
        );
      }

      i++;

      if (escaped) {
        escapes.push(i);
        escaped = false;
        return true;
      }

      if (ch === "\\") {
        escaped = true;
        return true;
      }

      if (ch === '"') {
        quotes.push(i++);
        return true;
      }

      return ch !== "'" && !this.isWhitespace(ch) && !this.isPunctuation(ch);
    });

    if (this.input.peek() === "'") {
      // end single quote is optional
      this.input.next();
    }

    let escapedString = rawString;

    for (let quote of quotes) {
      escapedString =
        escapedString.slice(0, quote) + "\\" + escapedString.slice(quote);
    }

    let removed = 0;

    for (let escape of escapes) {
      escape -= removed;
      const ch = escapedString.charAt(escape);

      if (
        ch !== '"' &&
        ch !== "\\" &&
        ch !== "/" &&
        ch !== "b" &&
        ch !== "f" &&
        ch !== "n" &&
        ch !== "r" &&
        ch !== "t" &&
        ch !== "u"
      ) {
        // drop the "\" preceeding
        escapedString =
          escapedString.slice(0, escape - 1) + escapedString.slice(escape);
        removed++;
      }
    }

    try {
      /**
       * @type {string}
       */
      const string = JSON.parse(`"${escapedString}"`);
      return { string, rawString, quote: "single" };
    } catch (e) {
      // throws
      this.input.error(e.message + ` "${escapedString}"`);
    }
  }

  /**
   * @param {string} ch
   */
  isWhitespace(ch) {
    return ch === " " || ch === "\t" || ch === "\n";
  }

  /**
   * @param {string} ch
   */
  isPunctuation(ch) {
    return (
      ch === "(" ||
      ch === ")" ||
      ch === "[" ||
      ch === "]" ||
      ch === "{" ||
      ch === "}" ||
      ch === ";"
    );
  }

  /**
   * @param {string} ch
   */
  isControlCharacter(ch) {
    const codePoint = ch.codePointAt(0);

    return codePoint <= 0x1f || (codePoint >= 0x7f && codePoint <= 0x9f);
  }

  /**
   * @param {function(string): boolean} predicate
   */
  while(predicate) {
    let str = "";

    while (!this.input.eof() && predicate(this.input.peek())) {
      str += this.input.next();
    }

    return str;
  }

  eof() {
    return this.peek().type === "eof";
  }

  *[Symbol.iterator]() {
    while (!this.eof()) {
      yield this.next();
    }
  }
}

class Ast {
  /**
   * @param {(Token | string)} type
   * @param {Object.<string, any>} [values={}]
   */
  constructor(type, values = {}) {
    if (type instanceof Token) {
      values = type;
    } else {
      this.type = type;
    }

    for (const [key, value] of Object.entries(values)) {
      this[key] = value;
    }
  }

  prettyPrint(depth = 0, position = 0) {
    const indent = "  ".repeat(depth);

    if (
      this.type === "atom" ||
      this.type === "boolean" ||
      this.type === "null" ||
      this.type === "punctuation"
    ) {
      return indent + this.value;
    }

    if (this.type === "number") {
      return indent + this.value.rawNumber;
    }

    if (this.type === "string") {
      return `${indent}"${this.value.rawString}"`;
    }

    if (this.type === "list" || this.type === "array") {
      let out = "";

      if (position > 0) {
        out += "\n";
      }

      out += indent;
      out += this.type === "list" ? "(" : "[";

      for (let i = 0; i < this.value.length; i++) {
        const ast = this.value[i];
        let newDepth = depth + 1;

        if (
          ast.type !== "list" &&
          ast.type !== "array" &&
          ast.type !== "object"
        ) {
          newDepth = 0;

          if (i > 0) {
            out += " ";
          }
        }

        out += ast.prettyPrint(newDepth, i);
      }

      if (this.closed) {
        out += this.type === "list" ? ")" : "]";
      }

      return out;
    }

    if (this.type === "object") {
      let out = "";

      if (depth > 0 && position > 0) {
        out += indent;
      }

      out += "{";

      for (let i = 0; i < this.value.length; i++) {
        const ast = this.value[i];

        out += "\n";
        out += ast.prettyPrint(depth + 1);
      }

      if (this.value.length > 0) {
        out += "\n" + indent;
      }

      if (this.closed) {
        out += "}";
      }

      return out;
    }

    if (this.type === "program") {
      let out = indent;

      for (let i = 0; i < this.value.length; i++) {
        if (i > 0) {
          out += "\n" + indent;
        }

        const ast = this.value[i];
        out += ast.prettyPrint(depth, i);
      }

      return out;
    }
  }
}

class Parser {
  /**
   *
   * @param {TokenStream} tokens
   */
  constructor(tokens) {
    this.tokens = tokens;
    this.macros = {};
  }

  parse() {
    const program = [];
    let expr = this.expression();

    while (expr.type !== "eof") {
      if (expr.type === "punctuation") {
        throw new Error(
          `Unexpected token (${expr.end[0]}:${expr.end[1]})\n${expr.prettyPrint(
            4
          )}`
        );
      }

      program.push(expr);
      expr = this.expression();
    }

    return new Ast("program", {
      value: program,
      start: [1, 0],
      end: expr.end,
    });
  }

  /**
   * @returns {Ast} AST representation of the next expression
   */
  expression() {
    let token = parser.tokens.next();

    while (token.type === "comment") {
      token = parser.tokens.next();
    }

    if (
      token.type === "atom" ||
      token.type === "boolean" ||
      token.type === "eof" ||
      token.type === "string" ||
      token.type === "null" ||
      token.type === "number"
    ) {
      return new Ast(token);
    }

    if (token.type === "punctuation") {
      if (token.value === "(") {
        const list = [];
        let expr = this.expression();

        while (expr.value !== ")") {
          if (expr.type === "eof") {
            const ast = new Ast("list", {
              value: list,
              start: token.start,
              end: expr.end,
              closed: false,
            });

            throw new Error(
              `Unexpected End Of File (${ast.start[0]}:${
                ast.start[1] + 1
              })\n${ast.prettyPrint(4)}`
            );
          }

          list.push(expr);
          expr = this.expression();
        }

        return new Ast("list", {
          value: list,
          start: token.start,
          end: expr.end,
          closed: true,
        });
      }

      if (token.value === "[") {
        const array = [];
        let expr = this.expression();

        while (expr.value !== "]") {
          if (expr.type === "eof") {
            const ast = new Ast("array", {
              value: array,
              start: token.start,
              end: expr.end,
              closed: false,
            });

            throw new Error(
              `Unexpected End Of File (${ast.start[0]}:${
                ast.start[1] + 1
              })\n${ast.prettyPrint(4)}`
            );
          }

          array.push(expr);
          expr = this.expression();
        }

        return new Ast("array", {
          value: array,
          start: token.start,
          end: expr.end,
          closed: true,
        });
      }

      if (token.value === "{") {
        const entries = [];
        let expr = this.expression();

        while (expr.value !== "}") {
          if (expr.type === "eof") {
            const ast = new Ast("object", {
              value: entries,
              start: token.start,
              end: expr.end,
              closed: false,
            });

            throw new Error(
              `Unexpected End Of File (${ast.start[0]}:${
                ast.start[1] + 1
              })\n${ast.prettyPrint(4)}`
            );
          }

          if (expr.type !== "list") {
            throw new Error(
              `Expected list in object, found ${expr.type} (${expr.start[0]}:${
                expr.start[1] + 1
              })\n${expr.prettyPrint(4)}`
            );
          }

          if (expr.value.length === 1) {
            if (expr.value[0].type !== "atom") {
              throw new Error(
                `Expected list in object with length 1 to be a name, but found ${expr.value[0].prettyPrint()} (${
                  expr.start[0]
                }:${expr.start[1] + 1})\n${expr.prettyPrint(4)}`
              );
            }
          } else if (expr.value.length !== 2) {
            throw new Error(
              `Expected list in object of length 2, but found length ${
                expr.value.length
              } (${expr.start[0]}:${expr.start[1] + 1})\n${expr.prettyPrint(4)}`
            );
          }

          entries.push(expr);
          expr = this.expression();
        }

        return new Ast("object", {
          value: entries,
          start: token.start,
          end: expr.end,
          closed: true,
        });
      }

      // ending punctuation
      return new Ast(token);
    }

    throw new Error(`Unexpected token: ${JSON.stringify(token, null, 2)}`);
  }
}

class Context {
  /**
   * @type {(Object.<string, function(Executer, Context)> | Object.<string, undefined>)>} built-in functions
   */
  static builtIns = {
    "*"(executer, ctx) {
      const expr = ctx.expr;
      const args = expr.value.values();
      // consume function name
      args.next();

      let result = 1;

      for (const arg of args) {
        result *= executer.execute(arg, ctx);
      }

      return result;
    },
    "/"(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 2) {
        throw new Error(
          `Expected at least one argument\n${expr.prettyPrint(4)}`
        );
      }

      if (expr.value.length === 2) {
        return 1 / executer.execute(expr.value[1], ctx);
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let result = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        result /= executer.execute(arg, ctx);
      }

      return result;
    },
    "+"(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 2) {
        throw new Error(
          `Expected at least one argument\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let result = executer.execute(args.next().value, ctx);

      if (typeof result !== "number") {
        result = result + "";
      }

      for (const arg of args) {
        result += executer.execute(arg, ctx);
      }

      return result;
    },
    "-"(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 2) {
        throw new Error(
          `Expected at least one argument\n${expr.prettyPrint(4)}`
        );
      }

      if (expr.value.length === 2) {
        return 0 - executer.execute(expr.value[1], ctx);
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let result = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        result -= executer.execute(arg, ctx);
      }

      return result;
    },
    "=="(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 3) {
        throw new Error(
          `Expected at least two arguments\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let first = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        if (first != executer.execute(arg, ctx)) {
          return false;
        }
      }

      return true;
    },
    "==="(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 3) {
        throw new Error(
          `Expected at least two arguments\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let first = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        if (first !== executer.execute(arg, ctx)) {
          return false;
        }
      }

      return true;
    },
    "&&"(executer, ctx) {
      const expr = ctx.expr;
      const args = expr.value.values();
      // consume function name
      args.next();

      for (const arg of args) {
        if (!executer.execute(arg, ctx)) {
          return false;
        }
      }

      return true;
    },
    "||"(executer, ctx) {
      const expr = ctx.expr;
      const args = expr.value.values();
      // consume function name
      args.next();

      for (const arg of args) {
        if (executer.execute(arg, ctx)) {
          return true;
        }
      }

      return false;
    },
    "<"(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 3) {
        throw new Error(
          `Expected at least two arguments\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let prev = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        const next = executer.execute(arg, ctx);

        if (prev >= next) {
          return false;
        }

        prev = next;
      }

      return true;
    },
    "<="(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 3) {
        throw new Error(
          `Expected at least two arguments\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let prev = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        const next = executer.execute(arg, ctx);

        if (prev > next) {
          return false;
        }

        prev = next;
      }

      return true;
    },
    ">"(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 3) {
        throw new Error(
          `Expected at least two arguments\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let prev = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        const next = executer.execute(arg, ctx);

        if (prev <= next) {
          return false;
        }

        prev = next;
      }

      return true;
    },
    ">="(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 3) {
        throw new Error(
          `Expected at least two arguments\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let prev = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        const next = executer.execute(arg, ctx);

        if (prev < next) {
          return false;
        }

        prev = next;
      }

      return true;
    },
    "="(executer, ctx) {
      return Context.builtIns.set(executer, ctx);
    },
    "."(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 2) {
        throw new Error(
          `Expected at least one arguments\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let result = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        if (result == null) {
          break;
        }

        const name = executer.execute(arg, ctx);
        result = result[name];
      }

      return result;
    },
    ".="(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 2) {
        throw new Error(
          `Expected at least one arguments\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let result = executer.execute(args.next().value, ctx);

      for (const arg of args) {
        if (result == null) {
          break;
        }

        const name = executer.execute(arg, ctx);
        result = result[name];
      }

      return result;
    },
    if(executer, ctx) {
      const expr = ctx.expr;
      const args = expr.value.slice(1);

      if (args.length < 2) {
        throw new Error(
          `Expected at least two arguments\n${expr.prettyPrint(4)}`
        );
      }

      if (args.length > 3) {
        throw new Error(
          `Expected at most three arguments\n${expr.prettyPrint(4)}`
        );
      }

      if (executer.execute(args[0], ctx)) {
        return executer.execute(args[1], ctx);
      }

      if (args.length === 3) {
        return executer.execute(args[2], ctx);
      }
    },
    let(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 2) {
        throw new Error(
          `Expected at least one argument\n${expr.prettyPrint(4)}`
        );
      }

      const args = expr.value.values();
      // consume function name
      args.next();

      let result;

      for (const arg of args) {
        if (arg.type !== "list") {
          throw new Error(
            `Arguments to "${this.name}" must be lists\n${expr.prettyPrint(4)}`
          );
        }

        if (arg.value.length !== 2) {
          throw new Error(
            `Arguments to "${
              this.name
            }" must be lists of length two\n${expr.prettyPrint(4)}`
          );
        }

        const name = arg.value[0];

        if (name.type !== "atom") {
          throw new Error(
            `Arguments to "${
              this.name
            }" must be lists with name and value\n${expr.prettyPrint(4)}`
          );
        }

        const value = executer.execute(arg.value[1], ctx);
        const error = ctx.let(name.value, value);

        if (error) {
          throw new Error(`${error.message}"\n${expr.prettyPrint(4)}`);
        }

        result = value;
      }

      return result;
    },
    set(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length !== 3) {
        throw new Error(`Expected two arguments\n${expr.prettyPrint(4)}`);
      }

      const name = expr.value[1];

      if (name.type !== "atom") {
        throw new Error(
          `Arguments to "${
            this.name
          }" must be name and value\n${expr.prettyPrint(4)}`
        );
      }

      const value = executer.execute(expr.value[2], ctx);
      const error = ctx.set(name.value, value);

      if (error) {
        throw new Error(`${error.message}"\n${expr.prettyPrint(4)}`);
      }

      return value;
    },
    func(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length < 3) {
        throw new Error(
          `Expected at least two arguments\n${expr.prettyPrint(4)}`
        );
      }

      let name = null;
      const args = expr.value.values();
      // consume function name
      args.next();

      let params = args.next().value;

      if (params.type === "atom") {
        // params is actually a name
        name = params.value;

        if (expr.value.length < 4) {
          throw new Error(
            `Expected at least three arguments for named ${
              this.name
            }\n${expr.prettyPrint(4)}`
          );
        }

        params = args.next().value;
      } else if (params.type !== "list") {
        throw new Error(
          `Expected first ${
            this.name
          } argument to be name or list\n${expr.prettyPrint(4)}`
        );
      }

      const fnParams = [];

      for (const param of params.value) {
        if (param.type !== "atom") {
          throw new Error(
            `Expected first ${
              this.name
            } argument to be list of names\n${expr.prettyPrint(4)}`
          );
        }

        if (ctx.find(param.value) === "built-in") {
          throw new Error(
            `Can't have ${this.name} parameter overwrite built-in "${
              param.name
            }"\n${expr.prettyPrint(4)}`
          );
        }

        if (param.value in fnParams) {
          throw new Error(
            `Duplicate paramter in ${this.name}"${
              param.name
            }"\n${expr.prettyPrint(4)}`
          );
        }

        fnParams.push(param.value);
      }

      const fnBody = [...args];

      const fn = (executer, ctx) => {
        const expr = ctx.expr;
        const args = expr.value.values();
        const fnName = args.next().value;
        const fnCtx = ctx.child(expr);

        // Set function arguments
        for (const param of fnParams) {
          const value = args.next().value;
          fnCtx.scope[param] = value && executer.execute(value, ctx);
        }

        // Execute any extra arguments supplied to the function
        for (const arg of args) {
          executer.execute(arg, ctx);
        }

        // Execute body of function and return last expression
        let result;
        for (const ast of fnBody) {
          result = executer.execute(ast, fnCtx);
        }

        return result;
      };

      if (name) {
        const error = ctx.let(name, fn);

        if (error) {
          throw new Error(`${error.message}"\n${expr.prettyPrint(4)}`);
        }
      }

      return fn;
    },
    macro(executer, ctx) {},
    typeof(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length !== 2) {
        throw new Error(`Expected one argument\n${expr.prettyPrint(4)}`);
      }

      const value = executer.execute(expr.value[1], ctx);

      if (Array.isArray(value)) {
        return "array";
      }

      if (value === null) {
        return "null";
      }

      return typeof value;
    },
    size(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length !== 2) {
        throw new Error(`Expected one argument\n${expr.prettyPrint(4)}`);
      }

      const value = executer.execute(expr.value[1], ctx);

      if (Array.isArray(value)) {
        return value.length;
      }

      if (value && typeof value === "object") {
        return Object.key(value).length;
      }

      throw new Error(
        `${this.name} expected array or object but found ${expr.prettyPrint()}`
      );
    },
    each(executer, ctx) {
      const expr = ctx.expr;

      if (expr.value.length !== 3) {
        throw new Error(`Expected 2 arguments\n${expr.prettyPrint(4)}`);
      }

      const fn = executer.execute(expr.value[2], ctx);

      if (typeof fn !== "function") {
        throw new Error(
          `Expected function as second item in ${this.name} (${expr.start[0]}:${
            expr.start[1] + 1
          })\n${expr.prettyPrint(4)}`
        );
      }

      const list = executer.execute(expr.value[1], ctx);
      let entries;

      if (Array.isArray(list)) {
        entries = list.entries();
      }

      if (!entries && list && typeof list === "object") {
        entries = Object.entries(list);
      }

      if (!entries) {
        throw new Error(
          `${
            this.name
          } expected array or object but found ${expr.prettyPrint()}`
        );
      }

      let result;
      for (const [key, value] of entries) {
        const args = new Ast("list", {
          value: [expr.value[2], key, value],
          start: [0, 0],
          end: [0, 0],
          closed: true,
        });

        result = fn.call(fn, executer, ctx.dupe(args));
      }

      return result;
    },
    print(executer, ctx) {
      const expr = ctx.expr;
      const args = expr.value.values();
      // consume function name
      args.next();

      let result = [];

      for (const arg of args) {
        result.push(executer.execute(arg, ctx));
      }

      console.log(...result);
    },
    debug(executer, ctx) {
      const expr = ctx.expr;
      const args = expr.value.values();
      // consume function name
      args.next();

      let code = [];
      let results = [];

      for (const arg of args) {
        code.push(arg.prettyPrint());
        results.push(executer.execute(arg, ctx));
      }

      code = code.join(" ");

      if (results.length > 0) {
        results.unshift("=");
      }

      console.log(code, ...results);
    },
    undefined: undefined,
  };

  constructor(expr, parent = null, scope = Object.create(null)) {
    this.expr = expr;
    this.parent = parent;
    this.scope = scope;
  }

  child(expr) {
    return new Context(expr, this);
  }

  dupe(expr) {
    return new Context(expr, this.parent, this.scope);
  }

  get(name) {
    if (Context.builtIns.hasOwnProperty(name)) {
      return { value: Context.builtIns[name] };
    }

    let ctx = this;

    do {
      if (name in ctx.scope) {
        return { value: ctx.scope[name] };
      }

      ctx = ctx.parent;
    } while (ctx);

    return false;
  }

  find(name) {
    if (Context.builtIns.hasOwnProperty(name)) {
      return "built-in";
    }

    let ctx = this;

    do {
      if (name in ctx.scope) {
        return ctx.scope;
      }

      ctx = ctx.parent;
    } while (ctx);

    return false;
  }

  let(name, value) {
    const where = this.find(name);

    if (where === "built-in") {
      return new Error(`Can't overwite built-in "${name}"`);
    }

    if (where) {
      return new Error(`Can't redeclare variable "${name}" (use "=" instead)`);
    }

    this.scope[name] = value;
  }

  set(name, value) {
    const where = this.find(name);

    if (where === "built-in") {
      return new Error(`Can't overwite built-in "${name}"`);
    }

    if (!where) {
      return new Error(`Can't find variable "${name}"`);
    }

    where[name] = value;
  }
}

class Executer {
  constructor(program) {
    this.program = program;
  }

  /**
   * @param {?Ast} expr
   * @param {?Context} ctx
   */
  execute(expr, ctx) {
    if (arguments.length === 0) {
      expr = this.program;
    }

    if (!(expr instanceof Ast)) {
      return expr;
    }

    if (expr.type === "boolean" || expr.type === "null") {
      return expr.value;
    }

    if (expr.type === "string") {
      return expr.value.string;
    }

    if (expr.type === "number") {
      return expr.value.number;
    }

    if (!ctx) {
      ctx = new Context(expr);
    } else {
      ctx = ctx.dupe(expr);
    }

    if (expr.type === "atom") {
      const value = ctx.get(expr.value);

      if (!value) {
        throw new Error(`Value not found: ${expr.value}`);
      }

      return value.value;
    }

    if (expr.type === "program") {
      let result;

      for (const ast of expr.value) {
        result = this.execute(ast, ctx);
      }

      return result;
    }

    if (expr.type === "array") {
      let result = [];

      for (const ast of expr.value) {
        result.push(this.execute(ast, ctx));
      }

      return result;
    }

    if (expr.type === "object") {
      let result = {};

      for (const ast of expr.value) {
        if (ast.value.length === 1) {
          const name = ast.value[0];
          result[name.value] = this.execute(name, ctx);
        } else {
          const [key, value] = ast.value;
          result[this.execute(key, ctx)] = this.execute(value, ctx);
        }
      }

      return result;
    }

    if (expr.type === "list") {
      if (expr.value.length === 0) {
        return null;
      }

      const fn = this.execute(expr.value[0], ctx);

      if (typeof fn !== "function") {
        throw new Error(
          `Expected function as first item in list (${expr.start[0]}:${
            expr.start[1] + 1
          })\n${expr.prettyPrint(4)}`
        );
      }

      return fn.call(fn, this, ctx);
    }
  }
}

const input = new CharacterStream(String.raw`
(func Person (name age) { (name) (age) })

(let
  (people [
    (Person "Jesse" 24)
    (Person 'James 22)
  ])
)

(each people (func (i val) (print val)))
`);

const tokens = new TokenStream(input);
const parser = new Parser(tokens);
const program = parser.parse();
const executer = new Executer(program);

// console.log(JSON.stringify(program));
console.log(program.prettyPrint());
console.log(executer.execute());
